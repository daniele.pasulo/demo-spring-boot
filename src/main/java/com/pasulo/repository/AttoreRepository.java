package com.pasulo.repository;

import com.pasulo.domain.Attore;
import org.springframework.stereotype.Repository;
import org.springframework.data.jpa.repository.JpaRepository;

@Repository
public interface AttoreRepository extends JpaRepository<Attore, Long>{
    
}
