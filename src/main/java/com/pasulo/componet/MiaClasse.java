package com.pasulo.componet;

import org.springframework.stereotype.Component;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString

@Component
public class MiaClasse {

    private String titolo;

    public MiaClasse(String titolo) {
        this.titolo = titolo;
    }

    public MiaClasse() {
    }
}