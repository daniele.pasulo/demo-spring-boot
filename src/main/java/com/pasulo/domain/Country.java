package com.pasulo.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Id;
import javax.persistence.Column;
import javax.persistence.OneToMany;
import javax.persistence.CascadeType;

@Entity
@Table(name = "countries")
@Getter @Setter @ToString
public class Country {

    @Id
    @Column(name = "country", length = 20, nullable = false)
    String country;


    public Country() {

    }
    public Country(String country) {
        this.country = country;
    }

    @OneToMany(mappedBy = "country", cascade = CascadeType.ALL, orphanRemoval = true)
    @JsonIgnore
    Set<Attore> attori;
}
