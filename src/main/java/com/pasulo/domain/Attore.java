package com.pasulo.domain;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIdentityReference;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Entity
@Table(name = "attori")
@Getter
@Setter
@ToString
public class Attore {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "cod_attore")
    Long codAttore;

    String nome;

    @Column(name = "anno_nascita")
    Long annoNascita;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "country")
    @JsonIdentityReference
    Country country;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "nota_id", referencedColumnName = "id")
    Nota nota;
}
