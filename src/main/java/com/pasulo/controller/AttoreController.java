package com.pasulo.controller;

import java.util.List;
import java.util.Optional;

import com.pasulo.componet.MiaClasse;
import com.pasulo.domain.Attore;
import com.pasulo.dto.AttoriPar;
import com.pasulo.service.AttoreService;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "/api/attori")
@CrossOrigin(origins = "*", methods = {RequestMethod.GET, RequestMethod.POST, RequestMethod.DELETE, RequestMethod.PUT}, maxAge = 3600)
public class AttoreController {

    // @Autowired
    AttoreService attoreService;
    MiaClasse miaClasse;

    public AttoreController(AttoreService service) {
        this.attoreService = service;
        this.miaClasse = new MiaClasse("Sono una classe");
    }

    @GetMapping(path = "/")
    public ResponseEntity<List<Attore>> attori(
        @RequestParam(required=false, name="page", defaultValue="0") int page,
        @RequestParam(required=false, name="size", defaultValue="10") int size,
        @RequestParam(required=false, name="ascdesc", defaultValue="asc") String ascdesc,
        @RequestParam(required=false, name="field", defaultValue="codAttore") String field
        ) {
        // int page = 0;
        // int size = 10;
        // String field = "codAttore";
        // String ascdesc = "asc";
        Sort sort = Sort.by(field);
        if (ascdesc.equals("desc")) {
            sort = sort.descending();
        }

        PageRequest request = PageRequest.of(page, size, sort);
        return new ResponseEntity<>(attoreService.findAll(request), HttpStatus.OK);
    }

    @GetMapping(path = "/c")
    public ResponseEntity<String> c() {
        return new ResponseEntity<>(miaClasse.getTitolo(), HttpStatus.OK);
    }

    @GetMapping(path = "/{codAttore}")
    public ResponseEntity<?> attore(@PathVariable Long codAttore) {
        Optional<Attore> attore = attoreService.find(codAttore);
        if (attore.isPresent()) {
            return new ResponseEntity<>(attore.get(), HttpStatus.OK);
        } else {
            return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping(path = "/")
    public ResponseEntity<Void> saveAttore(@RequestBody Attore a) {
        attoreService.save(a);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PostMapping(path="/dto/{id}")
    public void dto(@RequestBody AttoriPar par, @PathVariable Long id) {
        System.out.println(par);
    }

    @PutMapping(path = "/{codAttore}")
    public ResponseEntity<Attore> aggiornaAttore(@RequestBody Attore a) {
        Optional<Attore> attore = attoreService.update(a);
        return attore.map(value -> new ResponseEntity<>(value, HttpStatus.OK)).orElseGet(() -> new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @DeleteMapping(path = "/{codAttore}")
    public ResponseEntity<Void> deleteAttore(@PathVariable Long codAttore) {
        if (attoreService.delete(codAttore).isPresent()) {
            return new ResponseEntity<>(HttpStatus.OK);
        }
        else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}
