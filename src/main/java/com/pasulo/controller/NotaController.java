package com.pasulo.controller;

import com.pasulo.domain.Nota;
import com.pasulo.service.NotaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "/nota")
@CrossOrigin(origins = "*", methods = {RequestMethod.GET, RequestMethod.POST, RequestMethod.DELETE, RequestMethod.PUT}, maxAge = 3600)
public class NotaController {

    @Autowired
    NotaService notaService;

    @GetMapping()
    public ResponseEntity<?> getNote() {
        return new ResponseEntity<>(notaService.getAll(), HttpStatus.OK);
    }

    @PostMapping(path = "{codAttore}")
    public ResponseEntity<?> addNota(@PathVariable Long codAttore, @RequestBody Nota n) {
        notaService.save(n, codAttore);
        return new ResponseEntity<String>("Nota salvata", HttpStatus.OK);
    }
}
