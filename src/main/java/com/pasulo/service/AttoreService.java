package com.pasulo.service;

import java.util.List;
import java.util.Optional;

import com.pasulo.domain.Attore;
import com.pasulo.repository.AttoreRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

@Service
public class AttoreService {

    @Autowired
    AttoreRepository attoreRepository;

    public List<Attore> findAll() {
        return attoreRepository.findAll();
    }

    public Optional<Attore> find(Long id) {
        return attoreRepository.findById(id);
    }

    public void save(Attore a) {
        attoreRepository.save(a);
    }

    public Optional<Attore> delete(Long id) {
        Optional<Attore> attore = find(id);
        if (attore.isPresent()) {
            attoreRepository.deleteById(id);
            return attore;
        }
        else {
            return Optional.empty();
        }
    }

    public Optional<Attore> update(Attore a) {
        Optional<Attore> attore = attoreRepository.findById(a.getCodAttore());
        if (attore.isPresent()) {
            attoreRepository.save(a);
        }
        return attore;
    }
    
    public List<Attore> findAll(PageRequest p) {
        return attoreRepository.findAll(p).getContent();
    }
}
