package com.pasulo.service;

import com.pasulo.domain.Attore;
import com.pasulo.domain.Nota;
import com.pasulo.repository.AttoreRepository;
import com.pasulo.repository.NotaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class NotaService {
    @Autowired
    NotaRepository notaRepository;

    @Autowired
    AttoreRepository attoreRepository;

    public void save(Nota nota, Long codAttore) {
        notaRepository.save(nota);
        Optional<Attore> attore = attoreRepository.findById(codAttore);
        if (attore.isPresent()) {
            Attore a = attore.get();
            a.setNota(nota);
            attoreRepository.save(a);
        }
    }

    public List<Nota> getAll() {
        return notaRepository.findAll();
    }
}
