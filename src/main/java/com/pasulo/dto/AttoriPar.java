package com.pasulo.dto;

import lombok.Data;

@Data
public class AttoriPar {
    Long id;
    String nome;
}
