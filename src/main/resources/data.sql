
INSERT INTO note VALUES(1, "Nota 1");
INSERT INTO note VALUES(2, "Nota 2");
INSERT INTO note VALUES(3, "Nota 3");
INSERT INTO note VALUES(4, "Nota 4");
INSERT INTO note VALUES(5, "Nota 5");
INSERT INTO note VALUES(6, "Nota 6");
INSERT INTO note VALUES(7, "Nota 7");

INSERT INTO countries VALUES("Italia");
INSERT INTO countries VALUES("USA");
INSERT INTO countries VALUES("Germania");

INSERT INTO attori(anno_nascita, country, nome, nota_id) VALUES (1991, 'Italia', 'Mario', 1);
INSERT INTO attori(anno_nascita, country, nome, nota_id) VALUES (1975, 'Italia', 'Luigi', 4);
INSERT INTO attori(anno_nascita, country, nome, nota_id) VALUES (2004, 'Italia', 'Giovanni', 3);
INSERT INTO attori(anno_nascita, country, nome, nota_id) VALUES (2004, 'U.S.A', 'Martin', 7);
INSERT INTO attori(anno_nascita, country, nome, nota_id) VALUES (2004, 'U.S.A', 'Martin1', 5);
INSERT INTO attori(anno_nascita, country, nome, nota_id) VALUES (2004, 'U.S.A', 'Martin2', 2);
INSERT INTO attori(anno_nascita, country, nome, nota_id) VALUES (2004, 'U.S.A', 'Martin3', 6);