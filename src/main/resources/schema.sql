DROP DATABASE IF EXISTS databaseName;
CREATE DATABASE databaseName;
USE databaseName;

CREATE TABLE IF NOT EXISTS note
(
    id   BIGINT(20),
    note LONGTEXT,

    CONSTRAINT pk_id_note PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS countries
(
    country VARCHAR(20),

    CONSTRAINT pk_country PRIMARY KEY (country)
);

CREATE TABLE IF NOT EXISTS attori
(
    cod_attore   BIGINT(20) AUTO_INCREMENT,
    anno_nascita BIGINT(20),
    country      VARCHAR(20),
    nome         VARCHAR(255),
    nota_id      BIGINT(20),

    CONSTRAINT pk_cod_attore PRIMARY KEY (cod_attore),
    INDEX idx_nota_id_attore (nota_id ASC),
    INDEX idx_country_attore (country ASC),
    CONSTRAINT fk_nota_id_attore FOREIGN KEY (nota_id) REFERENCES note (id),
    CONSTRAINT fk_country_attore FOREIGN KEY (country) REFERENCES countries (country)
);