package com.pasulo.demo;

import com.pasulo.domain.Attore;
import com.pasulo.domain.Country;
import com.pasulo.domain.Nota;
import com.pasulo.repository.AttoreRepository;
import com.pasulo.repository.CountryRepository;
import com.pasulo.repository.NotaRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class DemoApplicationTests {

	@Autowired
	NotaRepository notaRepository;

	@Autowired
	AttoreRepository attoreRepository;
	@Autowired
	CountryRepository countryRepository;

	@Test
	void contextLoads() {
		Attore a = new Attore();
		a.setAnnoNascita(2000L);
		a.setNome("Brad Pitt");
		a.setCodAttore(1L);
		Country c = new Country();
		c.setCountry("USA");
		countryRepository.save(c);
		a.setCountry(c);
		Nota n = new Nota();
		n.setId(1L);
		n.setNote("Nota di test");
		notaRepository.save(n);
		a.setNota(n);
		attoreRepository.save(a);
	}
}
